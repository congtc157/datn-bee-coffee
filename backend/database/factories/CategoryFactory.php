<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Category>
 */
class CategoryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        static $index = 0;
        $names = ['Meet', 'Fish', 'Vegetable', 'Soft drink', 'Tech', 'Shirt', 'Trouser', 'Shoe', 'Fruit', 'Kitchenware'];

        return [
            'name' => $names[$index++ % count($names)],
        ];
    }
}