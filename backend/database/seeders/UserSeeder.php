<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
       
        $admin = new User();
        $admin = User::create([
            'id' => 2,
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => 12345678,
            'phone' => '0862460235',
        ]);
        $admin->roles()->attach(2);
        auth()->login($admin);

        $user = User::create([
            'id' => 3,
            'name' => 'User',
            'email' => 'user@gmail.com',
            'password' => 12345678,
            'phone' => '0862460235',
        ]);
        $user->roles()->attach(3);
        auth()->login($user);
    }
}
