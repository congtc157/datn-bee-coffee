@extends('dashboard.index')
<title>Edit Role</title>
@section('content')
<h1 style="text-align: :center">Update Role</h1>
<div class="container-fluid px-2 px-md-4">
    <div class="page-header min-height-300 border-radius-xl mt-4"
        style="background-image: url('https://images.unsplash.com/photo-1531512073830-ba890ca4eba2?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1920&q=80');">
        <span class="mask  bg-gradient-primary  opacity-6"></span>
    </div>
        <div class="card card-plain h-100">
                <form method='POST' action='{{ route('roles.update', $role->id ) }}'>
                    @csrf
                    @method('PUT')
                    <div class="row">    
                        <div class="mb-3 col-md-6">
                            <label class="form-label">Name</label>
                            <input type="text" name="name" class="form-control border border-2 p-2" value='{{ $role->name }}'>
                            @error('name')
                            <p class='text-danger inputerror'>{{ $message }} </p>
                            @enderror
                        </div>

                        <div class="mb-3 col-md-6">
                            <label class="form-label">Display Name</label>
                            <input type="text" name="display_name" class="form-control border border-2 p-2" value='{{ $role->display_name }}'>
                            @error('name')
                            <p class='text-danger inputerror'>{{ $message }} </p>
                            @enderror
                        </div>

                        <div class="mb-3 col-md-12">
                            <label class="form-label">Parent Categories</label>
                            @foreach($permissions as $permission)
                            <div class="custom-control custom-checkbox">
                                <input name="permissionIds[]" class="custom-control-input" type="checkbox"
                                       id="{{ $permission->name }}" value="{{ $permission->id }}" 
                                    @if($role->permissions->contains('id', $permission->id))
                                       checked
                                   @endif
                                >
                                <label for="{{ $permission->name }}"
                                       class="custom-control-label">{{ $permission->name }}</label>
                            </div>
                        @endforeach
                        </div>
                    </div>
                    <button type="submit" class="btn bg-gradient-dark">Submit</button>
                </form>

            </div>
        </div>
    </div>

</div>
@endsection