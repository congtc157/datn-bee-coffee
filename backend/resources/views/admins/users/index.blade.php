@extends('dashboard.index')
<title>List User</title>
@section('content')
<x-navbars.sidebar activePage="users"></x-navbars.sidebar>

<div class="container-fluid py-4">
            
    <div class="row">
        <div class="col-12">
            <div class="card my-4">
                <div class="card-tools float-left">
                    <form action="{{ route('users.index')}}" class="search-form d-flex">
                        @csrf
                        <div style="margin-left: 10px;  width :50%;" class="input-group input-group-outline mt-3 flex-grow-2" >
                            <input style="height: 50px; margin-right : 10px;"  type="text" class="form-control name" name="name" value="{{ request('name') }}" placeholder="Name">
                        </div>
                        <div style=" height:50px; margin-right: 10px; width :40%;" class="input-group input-group-outline mt-3">
                        <select class="role form-control category" name="role" id="role">
                            <option value="">Select Role...</option>
                            @foreach ($roles as $role)
                              <option value="{{ $role->id }}" {{ request('role') == $role->id ? 'selected' : '' }}>
                                {{ $role->name }}
                            </option> 
                            @endforeach
                        </select>  
                        </div>  
                        <div style="margin-top :15px;">
                            <button style="height: 50px; width:120px;"  type="submit" class="btn btn-primary">Search
                                <i class="fas fa-search"></i>
                              </button>
                        </div>
                    </form>
                  </div> 
                <div class=" me-3 my-3 text-end">
                    <form action="{{ route('users.create')}}">
                        <button type="submit" class="btn bg-gradient-dark mb-0" href="javascript:;"><i
                            class="material-icons text-sm">add</i>&nbsp;&nbsp;Add New
                        User</button>
                    </form>
                </div>
                <div class="card-body px-0 pb-2">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center mb-0">
                            <thead>
                                <tr>
                                    <th
                                        class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        #</th>
                                    <th
                                        class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                        Name</th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Email</th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Role</th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)                                       
                                <tr>
                                    <td>
                                        {{ $user->id}}
                                    </td>
                                    <td>
                                        {{ $user->name}}
                                    </td>
                                    <td>
                                        {{ $user->email}}
                                    </td>
                                    <td class="align-middle text-center text-sm">
                                        @if($user->roles)
                                            @foreach($user->roles as $role)
                                                <span class="badge badge-sm bg-gradient-success">                                                 
                                                    {!! $role->display_name . '<br>'!!}                                                
                                                </span>
                                            @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        <div class="mt-2 d-flex justify-content-center">
                                            <form action="{{ route('users.show', ['id' => $user->id] )}}">
                                                <button type="submit" rel="tooltip" class="btn btn-info btn-link btn btn-info me-2"
                                                    href="" data-original-title=""
                                                    title="">
                                                    <i class="material-icons">info</i>
                                                    <div class="ripple-container"></div>
                                                </button>  
                                            </form>
                                            <form action="{{ route('users.edit', ['id' => $user->id])}}">
                                                <button type="submit" rel="tooltip" class="btn btn-warning btn-link btn  me-2"
                                                href="" data-original-title=""
                                                title="">
                                                <i class="material-icons">edit</i>
                                                <div class="ripple-container"></div>
                                            </button>  
                                            </form>
                                            <form action="{{ route('users.destroy', ['id' => $user->id] )}}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button onclick="return confirm('Bạn có chắc chắn muốn xoá ?')" type="submit" class="btn btn-danger btn-link"
                                                data-original-title="" title="">
                                                <i class="material-icons">close</i>
                                                <div class="ripple-container"></div>
                                            </button>
                                            </form>
                                        </div>
                                    </td>      
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
 
    {{ $users->appends(request()->all())->links() }}
  
    <x-footers.auth></x-footers.auth>
</div>
@endsection