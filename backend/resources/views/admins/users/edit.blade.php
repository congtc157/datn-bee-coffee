@extends('pages.laravel-examples.user-profile')
<title>Edit User</title>

@section('content')

    <div class="card-body p-3">
                        <form method='POST' action='{{ route('users.update', $user->id) }}'>
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Email address</label>
                                    <input type="email" name="email" class="form-control border border-2 p-2" value='{{ $user->email }}' disabled>
                                    @error('email')
                                <p class='text-danger inputerror'>{{ $message }} </p>
                                @enderror
                                </div>
                                
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Name</label>
                                    <input type="text" name="name" class="form-control border border-2 p-2" value='{{ $user->name }}'>
                                    @error('name')
                                <p class='text-danger inputerror'>{{ $message }} </p>
                                @enderror
                                </div>
                               
                                <div class="mb-3 col-md-6">
                                    <label class="form-label">Phone</label>
                                    <input type="number" name="phone" class="form-control border border-2 p-2" value='{{ $user->phone }}'>
                                    @error('phone')
                                    <p class='text-danger inputerror'>{{ $message }} </p>
                                    @enderror
                                </div>

                                <div class="mb-3 col-md-6">
                                     <label class="form-label">Role</label>
                                    <div class="d-flex flex-wrap">
                                        @foreach($roles as $role)
                                            <div class="custom-control custom-radio me-3">
                                                <input name="roles" class="custom-control-input" type="radio" id="{{ $role->id }}" value="{{ $role->id }}"
                                                @if($role->users->contains('id', $user->id))
                                                    checked
                                                @endif>
                                                <label for="{{ $role->id }}" class="custom-control-label">{{ $role->name }}</label>
                                            </div>
                                        @endforeach
                                    </div>
                                    @error('roles')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>                            
                                
                                <div class="mb-3 col-md-12">
                                    <label for="floatingTextarea2">About</label>
                                    <textarea class="form-control border border-2 p-2"
                                        placeholder=" Say something about yourself" id="floatingTextarea2" name="about"
                                        rows="4" cols="50">{{ $user->about }}</textarea>
                                        @error('about')
                                        <p class='text-danger inputerror'>{{ $message }} </p>
                                        @enderror
                                </div>
                            </div>
                            <button type="submit" class="btn bg-gradient-dark">Submit</button>
                        </form>

    </div>
@endsection