@extends('dashboard.index')
<title>List Categories</title>
@section('content')
<x-navbars.sidebar activePage="categories"></x-navbars.sidebar>

<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card my-4">
                <div class="card-tools float-left">
                    <form action="{{ route('categories.index')}}"  class="search-form d-flex" method="GET">
                        @csrf

                        <div style="margin-left: 10px;  width :50%;" class="input-group input-group-outline mt-3">
                            <input style="height: 50px; margin-right : 10px;" type="text" class="form-control name" name="name" placeholder="Name..." value="{{ request('name') }}">
                        </div>
                        <div style=" height:50px; margin-right: 10px; width :40%;" class="input-group input-group-outline mt-3">
                        <select class="role form-control category" name="parent_id" id="parent_id">
                            <option value="">Select Parent Category...</option>
                            @foreach ($categoriesSearch as $category)
                              <option value="{{ $category->id }}" {{ request('parent_id') == $category->id ? 'selected' : '' }}>
                                {{ $category->name }}
                            </option> 
                            @endforeach
                        </select>  
                        </div>  
                        <div style="margin-top :15px;">
                            <button style="height: 50px; width:120px;"  type="submit" class="btn btn-primary">Search
                                <i class="fas fa-search"></i>
                              </button>
                        </div>
                    </form>

                  </div> 
                {{-- @hasPermission('categories.create', 'categories.store') --}}
                <div class=" me-3 my-3 text-end">
                    <form action="{{ route('categories.create')}}">
                        <button type="submit" class="btn bg-gradient-dark mb-0" href="javascript:;" href="{{ route('categories.create')}}"><i
                            class="material-icons text-sm">add</i>&nbsp;&nbsp;Add New
                        Categories</button>
                    </form>
                </div>
                {{-- @endhasPermission --}}
                <div class="card-body px-0 pb-2">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center mb-0">
                            <thead>
                                <tr>
                                    <th
                                        class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        #</th>
                                    <th
                                        class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                        Name</th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Parent Name</th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($categories as $category)                                       
                                <tr>
                                    <td>
                                        {{ $category->id}}
                                    </td>
                                    <td>
                                        {{ $category->name}}
                                    </td>
                                    <td class="align-middle text-center text-sm">
                                        @if($category->parent)
                                        <span class="badge badge-sm bg-gradient-success">                                                 
                                            {{ $category->parent->name }}                                                 
                                        </span>
                                        @endif
                                    </td>
                                    <td>
                                        <div class="mt-2 d-flex justify-content-center">
                                            {{-- @hasPermission('categories.show') --}}
                                            <form action="{{ route('categories.show', ['id' => $category->id] )}}">
                                                <button type="submit" rel="tooltip" class="btn btn-info btn-link btn btn-info me-2"
                                                    href="" data-original-title=""
                                                    title="">
                                                    <i class="material-icons">info</i>
                                                    <div class="ripple-container"></div>
                                                </button>  
                                            </form>
                                            {{-- @endhasPermission --}}
                                            {{-- @hasPermission('categories.edit', 'categories.update') --}}
                                            <form action="{{ route('categories.edit', ['id' => $category->id])}}">
                                                <button type="submit" rel="tooltip" class="btn btn-warning btn-link btn  me-2"
                                                href="" data-original-title=""
                                                title="">
                                                <i class="material-icons">edit</i>
                                                <div class="ripple-container"></div>
                                            </button>  
                                            </form>
                                            {{-- @endhasPermission --}}
                                            {{-- @hasPermission('categories.destroy') --}}
                                            <form action="{{ route('categories.destroy', ['id' => $category->id] )}}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button onclick="return confirm('Bạn có chắc chắn muốn xoá ?')" type="submit" class="btn btn-danger btn-link"
                                                data-original-title="" title="">
                                                <i class="material-icons">close</i>
                                                <div class="ripple-container"></div>
                                            </button>
                                            </form>
                                            {{-- @endhasPermission --}}
                                        </div>
                                    </td>      
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

        {{ $categories->appends(request()->all())->links() }}

    <x-footers.auth></x-footers.auth>
</div>
@endsection
