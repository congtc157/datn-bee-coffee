@extends('dashboard.index')
<script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
<title>List Product</title>

    <script>
        $(document).ready(function() {
           
            $('#category').select2({
            });
        });
    </script>
    <style>
        #category + .select2-container .select2-selection--single {
        height: 50px;
        border-color: #ccc;
        border-radius: 7px;
        padding-top: 10px;
        }
        #category + .select2-container .select2-selection--single .select2-selection__rendered {
        color: #999;
        }
        #category + .select2-container .select2-selection--single .select2-selection__arrow {
            top: 10px;
        }
    </style>

@section('content')
<x-navbars.sidebar activePage="products"></x-navbars.sidebar>

<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card my-4">
                <div class="card-tools float-left">
                    <form action="{{ route('products.index')}}" class="search-form d-flex " id="index" data-url="{{route('products.list')}}" method="POST">
                        @csrf
                        <div style="margin-left: 10px;  width :50%;" class="input-group input-group-outline mt-3 flex-grow-2" >
                          <input style="height: 50px; margin-right : 10px;" type="text" class="form-control form-control-sm name" name="name" value="{{ request('name') }}" placeholder="Name">
                        </div>
                        <div style="height: 50px; margin-right: 10px; width :40%;" class="input-group input-group-outline mt-3">
                          <select class=" role form-control form-control-sm category" name="category" id="category">
                            <option value=""> Select Category ...</option>
                            @foreach ($categories as $category)
                              <option value="{{ $category->id }}">{{ $category->name }}</option> 
                            @endforeach
                          </select>  
                        </div> 
                        <div style="margin-top :15px;">
                        <button style="height: 50px; width:120px;" type="button" class=" btn btn-primary btn-sm btn-search">Search
                            <i class="fas fa-search"></i>
                        </button> 
                        </div>
                        
                         
                      </form>
                  </div> 
                  @hasPermission('products.create', 'products.store')
                  <div class="card my-4">
                    <div class=" me-3 my-3 text-end">
                        <button type="button" class="btn bg-gradient-dark mb-0 product-action" data-bs-toggle="modal" data-bs-target="#exampleModal" data-url="{{ route('products.store')}}" data-action-name="create"><i
                            class="material-icons text-sm">add</i>&nbsp;&nbsp;Add New
                        Product</button>
                    </div>
                    @endhasPermission
                <div class="card-body px-0 pb-2">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center mb-0">
                            <thead>
                                <tr>
                                    <th
                                        class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        #</th>
                                    <th
                                        class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                        Name</th>
                                    <th
                                        class=" text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                        Price</th>
                                    <th
                                        class=" text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                        Categories</th>
                                    <th
                                        class=" text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-5">
                                        Image</th>
                                    <th
                                        class=" text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-7">
                                        Action</th>
                                </tr>
                            </thead>
                            <tbody class="table-body">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        @include('clients.products.form')
    </div>
    <ul class="pagination">  

    </ul>
    <x-footers.auth></x-footers.auth>
</div>

@endsection