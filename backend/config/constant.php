<?php

return [
    'folder_image' => [
        'products' => env('FOLDER_PRODUCT_IMAGE'),
    ],

    'image_default' => env('DEFAULT_IMAGE'),
];
