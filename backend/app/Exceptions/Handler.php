<?php

namespace App\Exceptions;

namespace App\Exceptions;

use Throwable;
use Illuminate\Http\Response;
use Illuminate\Database\QueryException;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }
    public function render($request, Throwable $e)
    {
        if ($e instanceof ValidationException && $request->ajax()) {
            return response()->json([
                'status_code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                'errors' => $e->validator->getMessageBag(),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        };
        if ($e instanceof AuthorizationException) {
            return redirect()->back()->withErrors([
                'warning' => $e->getMessage(),
            ]);
        };
        if ($e instanceof ModelNotFoundException || $e instanceof QueryException) {
            return response()->json([
                'status_code' => Response::HTTP_NOT_FOUND,
                'errors' => $e->getMessage(),
            ], Response::HTTP_NOT_FOUND);
        };
        return parent::render($request, $e);
    }
}
