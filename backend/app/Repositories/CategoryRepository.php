<?php

namespace App\Repositories;

use App\Models\Category;
use App\Repositories\BaseRepository;

class CategoryRepository extends BaseRepository
{
    public function model()
    {
        return Category::class;
    }

    public function search($data)
    {
        return $this->model->withNameCategory($data['name'] ?? null)
            ->withParent($data['parent_id'] ?? null)
            ->latest('id')
            ->paginate(10);
    }
    public function searchByName($name)
    {
    return $this->model->where('name', 'like', '%'.$name.'%')
        ->latest('id')
        ->paginate(10);
    }
}
