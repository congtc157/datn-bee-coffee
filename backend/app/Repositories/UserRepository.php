<?php
namespace App\Repositories;

use App\Models\User;

class UserRepository extends BaseRepository
{
    public function model(): string
    {
        return User::class;
    }
    public function search($data)
    {
        return $this->model->withName($data['name'] ?? null)
        ->withRole($data['role'] ?? null)
        ->latest('id')
        ->paginate(10);
    }
}
?>