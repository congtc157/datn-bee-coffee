<?php

namespace App\Http\Controllers;

use App\Http\Requests\Roles\StoreRoleRequest;
use App\Http\Requests\Roles\UpdateRoleRequest;
use Illuminate\Http\Request;
use App\Services\RoleService;

class RoleController extends Controller
{
    protected $roleService;

    public function __construct(RoleService $roleService)
    {
        $this->roleService = $roleService;
    }

    public function index(Request $request)
    {
        $roles = $this->roleService->search($request);
        return view('admins.roles.index', compact('roles'));
    }

    public function create()
    {
        $permissions = $this->roleService->listPermissions();
        return view('admins.roles.create', compact('permissions'));
    }

    public function store(StoreRoleRequest $request)
    {
        $this->roleService->store($request);
        return redirect()->route('roles.index')->with('success', 'Thêm role thành công');
    }

    public function show($id)
    {
        $role = $this->roleService->findOrFail($id);
        return view('admins.roles.show', compact(['role']));
    }

    public function edit($id)
    {
        $role = $this->roleService->findOrFail($id);
        $permissions = $this->roleService->listPermissions();
        return view('admins.roles.edit', compact(['role', 'permissions']));
    }

    public function update(UpdateRoleRequest $request, $id)
    {
        $this->roleService->update($request, $id);

        return redirect()->route('roles.index')->with('success', 'Thay đổi thông tin role thành công');
    }
    public function destroy($id)
    {
        $this->roleService->delete($id);
        return redirect()->route('roles.index')->with('success', 'Xoá role thành công');
    }
}
