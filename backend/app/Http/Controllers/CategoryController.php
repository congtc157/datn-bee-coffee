<?php

namespace App\Http\Controllers;

use App\Http\Requests\Category\StoreRequest;
use App\Http\Requests\Category\UpdateRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Services\CategoryService;

class CategoryController extends Controller
{
    protected $categoryService;
    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }
    public function index(Request $request)
    {
        $categories = $this->categoryService->search($request);
        $categoriesSearch = $this->categoryService->all();

        return view('clients.categories.index', compact('categories', 'categoriesSearch'));
    }
    public function create()
    {
        $categories = $this->categoryService->all();
        return view('clients.categories.create', compact('categories'));
    }
    public function store(StoreRequest $request)
    {
        $this->categoryService->create($request);

        return redirect()->route('categories.index')->with('success', 'Thêm Category thành công');
    }
    public function show($id)
    {
        $category = $this->categoryService->findOrFail($id);

        return view('clients.categories.show', compact('category'));
    }
    public function edit($id)
    {
        $categories = $this->categoryService->all();
        $category = $this->categoryService->findOrFail($id);

        return view('clients.categories.edit', compact('category', 'categories'));
    }
    public function update(UpdateRequest $request, $id)
    {
        $this->categoryService->update($request, $id);

        return redirect()->route('categories.index')->with('success', 'Thay đổi Category thành công');
    }
    public function destroy($id)
    {
        $this->categoryService->delete($id);

        return redirect()->route('categories.index')->with('success', 'Xoá Category thành công');
    }
    public function getListCategory(Request $request)
    {
        if ($request->wantsJson() && $request->has('q')) {
            $categories = $this->categoryService->searchByName($request->input('q'));
            $data = [];
            foreach ($categories as $category) {
                $data[] = [
                    'id' => $category->id,
                    'name' => $category->name,
                ];
            }
            return response()->json([
                'results' => $data,
            ]);
        } else {
            $categories = $this->categoryService->all();
        }
        return response()->json([
            'status_code' => Response::HTTP_OK,
            'results' => $categories,
            'data' => $categories
            ], Response::HTTP_OK);
    }
}